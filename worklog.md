# 2021
Week 34

- [ ] Setting up meeting with MEGR
- [X] Setup gitlab documentation project
- [X] Project log

 Week 35

- [ ] Nordcad beginners course

 Week 37

- [ ] Circuit research


 Week 38

- [X]  Nordcad workshop

 Week 39

- [ ] Course exercises overview

 Week 44

- [ ] Board outline

- [ ] Mounting holes using mechanical symbols

Week 46

- [ ] Component placement


Week 47

- [ ] Setup constraints in Orcad PCB editor

Week 48

- [ ] PCB rev
- [X] Course exercises overview
- [X] Documentation
 
Week 49

- [X] PCB rev
- [ ] Started on Report

 Week 50

- [X] Started on Report

# 2022
  Week 2

  - [X] Finish Report
